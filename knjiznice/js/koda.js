var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID ĹĄtevilke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
	var response = $.ajax({
		type: "POST",
		url: baseUrl + "/session?username=" + encodeURIComponent(username) +
			"&password=" + encodeURIComponent(password),
		async: false
	});
	return response.responseJSON.sessionId;
}

var genEhrId = ["8b0fabba-e768-4c11-9790-712812bfaa6b", "da24b093-ec18-4b40-8c13-0688c30522eb", "34129346-2b50-4db6-983d-379a58271039"];
var imena = ["Janez", "Ivan", "Nakljucko"];
var priimki = ["Novak", "Horvat", "Randome"];
var dnevi = ["1940-11-25T14:01", "1998-6-25T15:01", "1967-3-11T18:44"];
var meritve = [
	["2018-11-21T11:40Z", "80", "118", "92", "98"],
	["2018-11-21T11:40Z", "110", "132", "92", "97"],
	["2018-11-21T11:40Z", "95", "103", "92", "96"]
];



/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna ĹĄtevilka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */


function generirajPodatke(stPacienta, callback) {
	sessionId = getSessionId();
	var ehrId = "";
	var ime = imena[stPacienta - 1];
	var priimek = priimki[stPacienta - 1];
	var datumRojstva = dnevi[stPacienta - 1];

	if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
		priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
		$("#generiranoSporocilo").html("<span class='obvestilo label " +
			"label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	}
	else {
		$.ajaxSetup({
			headers: {
				"Ehr-Session": sessionId
			}
		});
		$.ajax({
			url: baseUrl + "/ehr",
			type: 'POST',
			success: function(data) {
				ehrId = data.ehrId;
				genEhrId[stPacienta - 1] = data.ehrId;
				var partyData = {
					firstNames: ime,
					lastNames: priimek,
					dateOfBirth: datumRojstva,
					partyAdditionalInfo: {
						key: "ehrId",
						value: ehrId
					}
				};
				$.ajax({
					url: baseUrl + "/demographics/party",
					type: 'POST',
					contentType: 'application/json',
					data: JSON.stringify(partyData),
					success: function(party) {
						if (party.action == 'CREATE') {
							$("#generiranoSporocilo").append("<span class='obvestilo " +
								"label label-success fade-in'>UspeĹĄno kreiran EHR '" +
								ehrId + "'.</span><br>");
						}
						callback(ehrId);
					},
					error: function(err) {
						$("#generiranoSporocilo").append("<span class='obvestilo label " +
							"label-danger fade-in'>Napaka '" +
							JSON.parse(err.responseText).userMessage + "'!<br>");
					}
				});
			}
		});
	}

	return ehrId;
}
window.onload = function() {
	document.getElementById("gengumb").onclick = function() {
		$("#generiranoSporocilo").html("");
		generirajPodatke(1, function(id1) {
			genEhrId[0] = id1;
			generirajPodatke(2, function(id2) {
				genEhrId[1] = id2;
				generirajPodatke(3, function(id3) {
					genEhrId[2] = id3;
				});
			});
		});
	};

	$('#preberiPredlogoBolnika').change(function() {
		var pid = parseInt($(this).val());
		$("#kreirajIme").val(imena[pid]);
		$("#kreirajPriimek").val(priimki[pid]);
		$("#kreirajDatumRojstva").val(dnevi[pid]);
	});
	$('#preberiObstojeciEHR').change(function() {
		$("#rezultatMeritveVitalnihZnakov").html("");
		d3.selectAll("svg > *").remove();
		var pid = parseInt($(this).val());
		$("#preberiEHRid").val(genEhrId[pid]);
	});

	$('#preberiObstojeciVitalniZnak').change(function() {
		var pid = parseInt($(this).val());
		$("#dodajVitalnoEHR").val(genEhrId[pid]);
		$("#dodajVitalnoDatumInUra").val(meritve[pid][0]);
		$("#dodajVitalnoTelesnaTeza").val(meritve[pid][1]);
		$("#dodajVitalnoKrvniTlakSistolicni").val(meritve[pid][2]);
		$("#dodajVitalnoKrvniTlakDiastolicni").val(meritve[pid][3]);
	});

	$("#izberiVitalniZnak").change(function() {
		$("#rezultatMeritveVitalnihZnakov").html("");
		d3.selectAll("svg > *").remove();
		var tip = $(this).val();
		preberiMeritveVitalnihZnakov(tip);

	});
};

function kreirajEHRzaBolnika() {
	sessionId = getSessionId();

	var ime = $("#kreirajIme").val();
	var priimek = $("#kreirajPriimek").val();
	var datumRojstva = $("#kreirajDatumRojstva").val();



	if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
		priimek.trim().length == 0 || datumRojstva.trim().length == 0 ||
		isNaN(ure) || isNaN(kaj) || isNaN(str) || str < 0 || kaj < 0 || ure < 0 || str > 100) {
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
			"label-warning fade-in'>Prosim pravilno vnesite zahtevane podatke!</span>");
	}
	else {
		$.ajaxSetup({
			headers: {
				"Ehr-Session": sessionId
			}
		});
		$.ajax({
			url: baseUrl + "/ehr",
			type: 'POST',
			success: function(data) {
				var ehrId = data.ehrId;
				var partyData = {
					firstNames: ime,
					lastNames: priimek,
					dateOfBirth: datumRojstva,
					partyAdditionalInfo: {
						key: "ehrId",
						value: ehrId
					}
				};
				$.ajax({
					url: baseUrl + "/demographics/party",
					type: 'POST',
					contentType: 'application/json',
					data: JSON.stringify(partyData),
					success: function(party) {
						if (party.action == 'CREATE') {
							$("#kreirajSporocilo").html("<span class='obvestilo " +
								"label label-success fade-in'>UspeĹĄno kreiran EHR '" +
								ehrId + "'.</span>");
							$("#preberiEHRid").val(ehrId);
						}
					},
					error: function(err) {
						$("#kreirajSporocilo").html("<span class='obvestilo label " +
							"label-danger fade-in'>Napaka '" +
							JSON.parse(err.responseText).userMessage + "'!");
					}
				});
			}
		});
	}
}

function preberiEHRodBolnika() {

	sessionId = getSessionId();

	var ehrId = $("#preberiEHRid").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiSporocilo").html("<span class='obvestilo label label-warning " +
			"fade-in'>Prosim vnesite zahtevan podatek!");
	}
	else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {
				"Ehr-Session": sessionId
			},
			success: function(data) {
				var party = data.party;
				var info = data.party.partyAdditionalInfo;
				var urTel, kaj, str;
				if (!urTel || !kaj || !str || !party.firstNames || !party.lastNames || !party.dateOfBirth) {
					$("#preberiSporocilo").html("<span class='obvestilo label label-warning " +
						"fade-in'>Bolnik nima pravilnih osebnih podatkov!");
				}
				else {

					$("#preberiSporocilo").html("\
		          <p><strong>Osnovni podatki</strong></p>\
		          <p><strong>Ime:</strong> " + party.firstNames + "</p>\
		          <p><strong>Priimek:</strong> " + party.lastNames + "</p>\
		          <p><strong>Datum in čas rojstva:</strong> " + party.dateOfBirth.replace("T", " ").replace("Z", "") + "</p>\
		          ");
				}
			},
			error: function(err) {
				$("#preberiSporocilo").html("<span class='obvestilo label " +
					"label-danger fade-in'>Napaka '" +
					JSON.parse(err.responseText).userMessage + "'!");
			}
		});
	}
}

function dodajMeritveVitalnihZnakov() {
	sessionId = getSessionId();

	var ehrId = $("#dodajVitalnoEHR").val();
	var datumInUra = $("#dodajVitalnoDatumInUra").val();
	var telesnaVisina = "0";
	var telesnaTeza = $("#dodajVitalnoTelesnaTeza").val();
	var telesnaTemperatura = "0";
	var sistolicniKrvniTlak = $("#dodajVitalnoKrvniTlakSistolicni").val();
	var diastolicniKrvniTlak = $("#dodajVitalnoKrvniTlakDiastolicni").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
			"label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	}
	else {
		$.ajaxSetup({
			headers: {
				"Ehr-Session": sessionId
			}
		});
		var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
			// https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
			"ctx/language": "en",
			"ctx/territory": "SI",
			"ctx/time": datumInUra,
			"vital_signs/height_length/any_event/body_height_length": telesnaVisina,
			"vital_signs/body_weight/any_event/body_weight": telesnaTeza,
			"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
			"vital_signs/body_temperature/any_event/temperature|unit": "Â°C",
			"vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
			"vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,

		};
		var parametriZahteve = {
			ehrId: ehrId,
			templateId: 'Vital Signs',
			format: 'FLAT'
		};
		$.ajax({
			url: baseUrl + "/composition?" + $.param(parametriZahteve),
			type: 'POST',
			contentType: 'application/json',
			data: JSON.stringify(podatki),
			success: function(res) {
				$("#dodajMeritveVitalnihZnakovSporocilo").html(
					"<span class='obvestilo label label-success fade-in'>" +
					res.meta.href + ".</span>");
			},
			error: function(err) {
				$("#dodajMeritveVitalnihZnakovSporocilo").html(
					"<span class='obvestilo label label-danger fade-in'>Napaka '" +
					JSON.parse(err.responseText).userMessage + "'!");
			}
		});
	}
}

var readData = [];
var mapData = [];

function preberiMeritveVitalnihZnakov(tipZnaka) {
	sessionId = getSessionId();

	var ehrId = $("#preberiEHRid").val();
	var tip = tipZnaka;


	if (!ehrId || ehrId.trim().length == 0 || !tip || tip.trim().length == 0) {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
			"label label-warning fade-in'>Prosim vnesite zahtevan podatek in nato izberite vitalni znak!");
	}
	else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {
				"Ehr-Session": sessionId
			},
			success: function(data) {
				var party = data.party;
				$("#rezultatMeritveVitalnihZnakov").html("<br/><span>Pridobivanje " +
					"podatkov za <b>'" + tip + "'</b> bolnika <b>'" + party.firstNames +
					" " + party.lastNames + "'</b>.</span><br/><br/>");
				if (tip == "Telesna teža") {
					$.ajax({
						url: baseUrl + "/view/" + ehrId + "/" + "weight",
						type: 'GET',
						headers: {
							"Ehr-Session": sessionId
						},
						success: function(res) {
							if (res.length > 0) {
								var results = "<table class='table table-striped " +
									"table-hover'><tr><th>Datum in ura</th>" +
									"<th class='text-right'>Telesna teža</th></tr>";
								readData = [];
								mapData = [];
								for (var i in res) {
									readData.push(res[i].weight);
									mapData.push(res[i].time);
									results += "<tr><td>" + res[i].time +
										"</td><td class='text-right'>" + res[i].weight + " " +
										res[i].unit + "</td>";
								}
								drawGraph();
								results += "</table>";
								$("#rezultatMeritveVitalnihZnakov").append(results);
							}
							else {
								$("#preberiMeritveVitalnihZnakovSporocilo").html(
									"<span class='obvestilo label label-warning fade-in'>" +
									"Ni podatkov!</span>");
							}
							$("#rezultatMeritveVitalnihZnakov").append("<p>Meritve so prikazane po vrsti v grafu:</p>");

						},
						error: function() {
							$("#preberiMeritveVitalnihZnakovSporocilo").html(
								"<span class='obvestilo label label-danger fade-in'>Napaka '" +
								JSON.parse(err.responseText).userMessage + "'!");
						}
					});
				}
				if (tip == "Krvni tlak") {
					$.ajax({
						url: baseUrl + "/view/" + ehrId + "/" + "blood_pressure",
						type: 'GET',
						headers: {
							"Ehr-Session": sessionId
						},
						success: function(res) {
							if (res.length > 0) {
								var results = "<table class='table table-striped " +
									"table-hover'><tr><th>Datum in ura</th>" +
									"<th class='text-right'>Krvni tlak</th></tr>";
								readData = [];
								mapData = [];
								for (var i in res) {
									readData.push((res[i].systolic + res[i].diastolic) / 2);
									mapData.push(res[i].time);
									results += "<tr><td>" + res[i].time +
										"</td><td class='text-right'>Sistolični: " + res[i].systolic + " " +
										res[i].unit + " || " + " Diastolični: " + res[i].diastolic + " " + res[i].unit + "</td>";
								}
								drawGraph();
								results += "</table>";
								$("#rezultatMeritveVitalnihZnakov").append(results);
							}
							else {
								$("#preberiMeritveVitalnihZnakovSporocilo").html(
									"<span class='obvestilo label label-warning fade-in'>" +
									"Ni podatkov!</span>");
							}
							$("#rezultatMeritveVitalnihZnakov").append("<p>Meritve (povprečje tlakov) so prikazane po vrsti v grafu:</p>");

						},
						error: function() {
							$("#preberiMeritveVitalnihZnakovSporocilo").html(
								"<span class='obvestilo label label-danger fade-in'>Napaka '" +
								JSON.parse(err.responseText).userMessage + "'!");
						}
					});
				}
			},
			error: function(err) {
				$("#preberiMeritveVitalnihZnakovSporocilo").html(
					"<span class='obvestilo label label-danger fade-in'>Napaka '" +
					JSON.parse(err.responseText).userMessage + "'!");
			}
		});
	}
}


function drawGraph() {

	var max = 0;
	for (var i = 0; i < readData.length; i++)
		if (parseFloat(readData[i]) > max)
			max = parseFloat(readData[i]);
	if (isNaN(max) || max == 0)
		max = 1;

	var data = [];

	for (var i = 0; i < readData.length; i++) {
		data.push({
			letter: i + 1,
			frequency: parseFloat(readData[i])
		});
	}

	d3.selectAll("svg > *").remove();
	var svg = d3.select("svg"),
		margin = {
			top: 20,
			right: 20,
			bottom: 30,
			left: 40
		},
		width = +svg.attr("width") - margin.left - margin.right,
		height = +svg.attr("height") - margin.top - margin.bottom;
	var x = d3.scaleBand().rangeRound([0, width]).padding(0.1),
		y = d3.scaleLinear().rangeRound([height, 0]);

	var g = svg.append("g")
		.attr("transform", "translate(" + margin.left + "," + margin.top + ")");



	x.domain(data.map(function(d) {
		return d.letter;
	}));
	y.domain([0, max]);

	g.append("g")
		.attr("class", "axis axis--x")
		.attr("transform", "translate(0," + height + ")")
		.call(d3.axisBottom(x));

	g.append("g")
		.attr("class", "axis axis--y")
		.call(d3.axisLeft(y).ticks(10))
		.append("text")
		.attr("transform", "rotate(-90)")
		.attr("y", 6)
		.attr("dy", "0.71em")
		.attr("text-anchor", "end")
		.text("Podatki");

	g.selectAll(".bar")
		.data(data)
		.enter().append("rect")
		.attr("class", "bar")
		.attr("x", function(d) {
			return x(d.letter);
		})
		.attr("y", function(d) {
			return y(d.frequency);
		})
		.attr("width", x.bandwidth())
		.attr("height", function(d) {
			return height - y(d.frequency);
		});

}




function initMap() {
	var pyrmont;

	function createMap() {
		var map;
		var infowindow;
		map = new google.maps.Map(document.getElementById('map'), {
			center: pyrmont,
			zoom: 13
		});

		infowindow = new google.maps.InfoWindow();
		var service = new google.maps.places.PlacesService(map);
		service.nearbySearch({
			location: pyrmont,
			radius: 4000,
			type: ['gym']
		}, function(results, status) {
			if (status === google.maps.places.PlacesServiceStatus.OK) {
				for (var i = 0; i < results.length; i++) {
					place = results[i];
					var placeLoc = place.geometry.location;
					var marker = new google.maps.Marker({
						map: map,
						position: place.geometry.location
					});
					google.maps.event.addListener(marker, 'click', function() {
						infowindow.setContent(place.name);
						infowindow.open(map, this);
					});
				}
			}
		});
	}

	var posLat, posLon;
	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(function(pos) {
			posLat = pos.coords.latitude;
			posLon = pos.coords.longitude;
			pyrmont = {
				lat: posLat,
				lng: posLon
			};
			createMap();
		}, function(err) {
			pyrmont = {
				lat: 46.056946,
				lng: 14.505751
			};
			createMap();
		});
	}
	else {
		pyrmont = {
			lat: 46.056946,
			lng: 14.505751
		};
		createMap();
	}



}